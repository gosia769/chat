import org.eclipse.jetty.websocket.api.*;
import org.eclipse.jetty.websocket.api.annotations.*;

/**
 * Created by Gosia on 17/01/2017.
 */
@WebSocket
public class ChatWebSocketHandler {
    private String sender, msg;
    private Chat chat;

    public ChatWebSocketHandler(Chat chat) {
        this.chat = chat;
    }

    @OnWebSocketConnect
    public void onConnect(Session user) throws Exception {
        String username = Chat.getUsername(user);
        chat.userUsernameMap.put(user, username);
        chat.sendJSON(user);
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        chat.leaveChannel(user);
        chat.userUsernameMap.remove(user);

    }

    @OnWebSocketMessage
    public void onMessage(Session user, String message) {
        chat.checkMessage(user, message);
    }
}
