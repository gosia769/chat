import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Gosia on 20/01/2017.
 */
// operacje na kanale, lista uzytkonwkow konkretnego kanalu
public class Channel {

    private Map<Session, String> channelUsersMap = new ConcurrentHashMap<>(); //mapa urzytkowniow na jednym kanale
    private String channelName;
    private Chat chat;

    public Channel(String channelName, Chat chat) {
        this.channelName = channelName;
        this.chat = chat;
    }

    public void addUser(Session user) {
        channelUsersMap.put(user, Chat.getUsername(user));
    }

    public void removeUser(Session user) {
        channelUsersMap.remove(user);
    }


    public void broadcastMessageOnChannel(String sender, String message) {
        channelUsersMap.keySet().stream().filter(Session::isOpen).forEach(session -> {
            try {
                session.getRemote().sendString(String.valueOf(new JSONObject()
                        .put("channel", "true")
                        .put("userMessage", chat.createHtmlMessageFromSender(sender, message))
                        .put("userlist", channelUsersMap.values())
                ));
            } catch (JSONException | IOException e) {
                System.out.println(e.getMessage());
            }
        });
    }
}
