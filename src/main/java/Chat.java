import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpCookie;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static j2html.TagCreator.*;
import static spark.Spark.*;

public class Chat {

    public Map<Session, String> userUsernameMap = new ConcurrentHashMap<>();        // this map is shared between sessions and threads, so it needs to be thread-safe (http://stackoverflow.com/a/2688817)
    private Map<Session, Channel> sessionChanelMap = new ConcurrentHashMap<>();     // mapa uzytkownik - kalnal
    private Map<String, Channel> channelnameChannelMap = new ConcurrentHashMap<>(); // mapa nazwa kanalu - kanal
    private ChatWebSocketHandler handler;

    public Chat() {
        this.handler = new ChatWebSocketHandler(this);
        channelnameChannelMap.put("ChatBot", new ChatBot("chatbot", this));
    }

    public static void main(String[] args) {
        Chat chat = new Chat();
        chat.init();
    }

    private void init() {
        staticFiles.location("/public"); //index.html is served at localhost:4567 (default port)
        staticFiles.expireTime(600);
        webSocket("/chat", handler);
        get("/chat", (request, response) -> {
            if (request.cookie("username") == null) {
                response.redirect("/");
            } else {
                response.redirect("/chat.html");
            }
            return null;
        });
    }


    public static String getUsername(Session user) {
        return user
                .getUpgradeRequest()
                .getCookies()
                .stream()
                .filter(p -> p.getName().equals("username"))
                .map(HttpCookie::getValue)
                .findFirst().orElse(" ");
    }

    //wiadomosci wysylane przez websocket
    public void checkMessage(Session user, String message) {
        if (message.startsWith("-message")) {
            sendMessageOnChannel(user, message);
        } else if (message.startsWith("-leave")) {
            leaveChannel(user);
        } else if (message.startsWith("-join")) {
            joinChannel(user, message);
        } else if (message.startsWith("-create")) {
            createChannel(user, message);
        }
    }

    //znajduje na jakim kanale jest i wywoluje metode brodcastmessageonchannel
    private void sendMessageOnChannel(Session user, String message) {
        String msg = message.substring(9, message.length());
        sessionChanelMap.get(user).broadcastMessageOnChannel(Chat.getUsername(user), msg);
    }

    private void joinChannel(Session user, String message) {
        String channelname = message.substring(5, message.length());        //nazwa kanalu
        Channel channel = channelnameChannelMap.get(channelname);           //znajdz kanal o podanej nazwie
        channel.addUser(user);                                              //dodaj do listy osob na danym kanale osobe chcaca dolaczyc
        sessionChanelMap.put(user, channel);
        channel.broadcastMessageOnChannel("Server",
                 userUsernameMap.get(user) + " joined the channel");   //wyswetl wiadomosc o dolaczeniu do kanalu nowego uzytkownika
    }

    void leaveChannel(Session user) {
        Channel chan =  sessionChanelMap.get(user);
        chan.removeUser(user);    //usuwam uzytkownika z kanau
        chan.broadcastMessageOnChannel("Server", "User " + userUsernameMap.get(user)+" left the channel");
        sessionChanelMap.remove(user);                  //usuwam uzytkownika z sessionChannelMap
        sendJSON(user);
    }

    private void createChannel(Session user, String message) {
        String channelName = (message.substring(7, message.length()));
        Channel newChan = new Channel(channelName, this);
        sessionChanelMap.put(user, newChan);
        channelnameChannelMap.put(channelName, newChan);
        sendJSON(user);
    }

    void sendJSON(Session user) {
        try {
            user.getRemote().sendString(String.valueOf(new JSONObject()
                    .put("channel", "false")
                    .put("channellist", channelnameChannelMap.keySet())
            ));
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    //Builds a HTML element with a sender-name, a message, and a timestamp,
    public String createHtmlMessageFromSender(String sender, String message) {
        return article().with(
                b(sender + " says:"),
                p(message),
                span().withClass("timestamp").withText(new SimpleDateFormat("HH:mm:ss").format(new Date()))
        ).render();
    }
}