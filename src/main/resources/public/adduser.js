/**
 * Created by Gosia on 20/01/2017.
 */

//add user if "add user" is clicked
id("joinchat").addEventListener("click", function () {
    document.cookie = "username=" + id("joinchat").value;
    location.href = "chat.html";
});

//Send message if enter is pressed in the input field
id("username").addEventListener("keypress", function (e) {
    if (e.keyCode === 13) {
        document.cookie = "username=" + (e.target.value);
        location.href = "chat.html";
        //console.log("asd")
    }
});

//Helper function for selecting element by id
function id(id) {
    return document.getElementById(id);
}