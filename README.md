# README #

### What is this repository for? ###

Implementacja aplikacji webowej, korzystając z biblioteki Spark.

Aplikację webową, która posiada następujące cechy:
-po wejściu na stronę startową aplikacja pytać użytkownika o imię i zapisuje je w cookie
-po podaniu imienia użytkownika wyświetla się lista dostępnych kanałów
-użytkownik klikając na nazwę kanału dołącza do konwersacji prowadzonej na tym kanale
-pod listą kanałów dostępny jest guzik "Utwórz nowy kanał"
-utworzenie nowego kanału powoduje, że dany kanał staje się dostępny dla innych (wyświetla się na liście dostępnych kanałów - po wejściu na główną stronę aplikacji)
-dołączenie do kanału umożliwia wysyłanie wiadomości - wiadomość ta jest dostarczana do wszystkich osób, które są na danym kanale
-na kanale jest guzik pozwalający na opuszczenie kanału
-aplikacja posiada wbudowany kanał "chatbot"
-chatbot udziela odpowiedzi na następujące pytania:
	-Która godzina?
	-Jaki dziś dzień tygodnia?
	-Jaka jest pogoda w Krakowie?
-Realizacja chatu opiera się o mechanizm WebSocketów.
-Restart serwera wymazuje listę wszystkich kanałów. Tzn. kanały i wiadomości nie mają być zapisywane w żadnej bazie danych.